if input('Для использования программы необходимо установить библиотеку scipy. После установки введите \'y\' для продолжения: ') == 'y':
	from scipy.stats import t

	data = [float(i[:-1]) for i in open(input('Введите название (без расширения) файла с выборкой: ') + '.csv')]
	E = sum(data) / len(data)
	S0 = (len(data) * (sum([i ** 2 for i in data]) / len(data) - E ** 2) / (len(data) - 1)) ** 0.5
	r = abs(len(data) ** 0.5 * (E - float(input('Введите ожидаемый вес одного эскимо: '))) / S0)
	try:
		from scipy.stats import t
		c = t.ppf(1 - float(input('Введите уровень значимости \u03B5: ')) / 2, len(data) - 1)
	except ModuleNotFoundError:
		print('Из таблицы найдите квантиль уровня (1 - \u03B5/2) (one-tailed test) распределения Стьюдента с параметром (n-1),\nгде n - размер выборки:\nru.wikipedia.org/wiki/%D0%9A%D0%B2%D0%B0%D0%BD%D1%82%D0%B8%D0%BB%D0%B8_%D1%80%D0%B0%D1%81%D0%BF%D1%80%D0%B5%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F_%D0%A1%D1%82%D1%8C%D1%8E%D0%B4%D0%B5%D0%BD%D1%82%D0%B0')
		c = float(input('Введите квантиль: '))
		

	print('-' * 50)
	print('Выборочное среднее: ' + str(E))
	print('Оценка среднеквадратичного отклонения: ' + str(S0))
	print('Значение модуля функции отклонения: ' + str(r))
	if r < c:
		print('Гипотеза верна')
	else:
		print('Гипотеза не верна')