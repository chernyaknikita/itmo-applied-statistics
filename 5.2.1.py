print('Форма ввода: cx^n; x∈[a,b]')

n, a, b = int(input('Введите n: ')), int(input('Введите a: ')), int (input('Введите b: '))


C = ((n+1)/(b**(n+1)-a**(n+1)))

E = ((C/(n+2))*((b**(n+2))-(a**(n+2))))

D = ((C/(n+3))*((b**(n+3))-(a**(n+3)))-(E**2))

print('C:', C)
print('E:', E)
print('D:', D)
